
from string import punctuation, digits
import random
import numpy as np

# Part I
def get_order(n_samples):
    try:
        with open(str(n_samples) + '.txt') as fp:
            line = fp.readline()
            return list(map(int, line.split(',')))
            

    except FileNotFoundError:
        random.seed(1)
        indices = list(range(n_samples))
        random.shuffle(indices)
        return indices


def extract_words(input_string):
    """
    Helper function for bag_of_words()
    Inputs a text string
    Returns a list of lowercase words in the string.
    Punctuation and digits are separated out into their own words.
    """
    for c in punctuation + digits:
        input_string = input_string.replace(c, ' ' + c + ' ')

    return input_string.lower().split()


def bag_of_words(path_data, texts):
    """
    Inputs a list of string reviews
    Returns a dictionary of unique unigrams occurring over the input

    Feel free to change this code as guided by Problem 9
    """
    # Your code here
    # Load stopwords.txt files in a list
    file1 = open(path_data+'/stopwords.txt', 'r')
    stopwords = file1.readlines()
    #print(stopwords)
    # Strips the newline character
    count = 0
    for line in stopwords:
        stopwords[count] = line.split('\n')[0] 
        count += 1
    #print(stopwords)
    #13234 - 126 = 13108
    dictionary = {} # maps word to unique index
    for text in texts:
        word_list = extract_words(text)
        for word in word_list:
            if word not in dictionary:
                # No toma en cuenta las palabras de stopwords dict in stopwords.txt file
                if word not in stopwords:
                    dictionary[word] = len(dictionary)
    return dictionary


def extract_bow_feature_vectors(reviews, dictionary):
    """
    Inputs a list of string reviews
    Inputs the dictionary of words as given by bag_of_words
    Returns the bag-of-words feature matrix representation of the data.
    The returned matrix is of shape (n, m), where n is the number of reviews
    and m the total number of entries in the dictionary.

    Feel free to change this code as guided by Problem 9
    
    # MIT
    Even if you use the original feature sets (without stop word removal), it will still decrease performance.

    It is possible that giving models more information leads to worse performance, when model overfits irrelevant information.

    From the learning perspective, it is helpful to somehow “regularize" feature representations (e.g., binarizing them).

    """
    # Your code here

    num_reviews = len(reviews)
    feature_matrix = np.zeros([num_reviews, len(dictionary)])

    for i, text in enumerate(reviews):
        word_list = extract_words(text)
        for word in word_list:
            if word in dictionary:
                # Binary Features
                #feature_matrix[i, dictionary[word]] = 1
                # Change Binary Features to Counts Features 
                feature_matrix[i, dictionary[word]] += 1
    return feature_matrix

