from pipeline.classification import classify

def classifier_accuracy(
        classifier,
        train_feature_matrix,
        val_feature_matrix,
        train_labels,
        val_labels,
        **kwargs):
    """
    Trains a linear classifier and computes accuracy.
    The classifier is trained on the train data. The classifier's
    accuracy on the train and validation data is then returned.

    Args:
        classifier - A classifier function that takes arguments
            (feature matrix, labels, **kwargs) and returns (theta, theta_0)
        train_feature_matrix - A numpy matrix describing the training
            data. Each row represents a single data point.
        val_feature_matrix - A numpy matrix describing the validation
            data. Each row represents a single data point.
        train_labels - A numpy array where the kth element of the array
            is the correct classification of the kth row of the training
            feature matrix.
        val_labels - A numpy array where the kth element of the array
            is the correct classification of the kth row of the validation
            feature matrix.
        **kwargs - Additional named arguments to pass to the classifier
            (e.g. T or L)

    Returns: A tuple in which the first element is the (scalar) accuracy of the
    trained classifier on the training data and the second element is the
    accuracy of the trained classifier on the validation data.
    """

        # train the given classifier using the training data 
        # compute compute the classification accuracy on both the train and validation data.
        #The return values should be a tuple where the first value is the training accuracy and the second value is the validation accuracy. 
    
    #print("Train")
    train_theta, train_theta0 = classifier(train_feature_matrix, train_labels, **kwargs)
    #print(train_feature_matrix, train_labels)
    #print(train_theta,train_theta0)
    train_results = classify(train_feature_matrix, train_theta, train_theta0) 
    #print(train_results)
    #print(train_labels)
    #print(accuracy(train_results, train_labels))
    
    #print("Val")
    #print(val_feature_matrix, val_labels)
    val_results = classify(val_feature_matrix, train_theta, train_theta0) 
    #print(val_results)
    #print(val_labels)
    #print(accuracy(val_results, val_labels))
    
    return accuracy(train_results, train_labels), accuracy(val_results, val_labels)

         
def accuracy(preds, targets):
    """
    Given length-N vectors containing predicted and target labels,
    returns the percentage and number of correct predictions.
    """
    return (preds == targets).mean()