
import numpy as np
import pipeline.preprocessing as prepro
from models.perceptron import perceptron_single_step_update
def average_perceptron(feature_matrix, labels, T):
    """
    Runs the average perceptron algorithm on a given set of data. Runs T
    iterations through the data set, there is no need to worry about
    stopping early.

    NOTE: Please use the previously implemented functions when applicable.
    Do not copy paste code from previous parts.

    NOTE: Iterate the data matrix by the orders returned by get_order(feature_matrix.shape[0])


    Args:
        feature_matrix -  A numpy matrix describing the given data. Each row
            represents a single data point.
        labels - A numpy array where the kth element of the array is the
            correct classification of the kth row of the feature matrix.
        T - An integer indicating how many times the perceptron algorithm
            should iterate through the feature matrix.

    Returns: A tuple where the first element is a numpy array with the value of
    the average theta, the linear classification parameter, found after T
    iterations through the feature matrix and the second element is a real
    number with the value of the average theta_0, the offset classification
    parameter, found after T iterations through the feature matrix.

    Hint: It is difficult to keep a running average; however, it is simple to
    find a sum and divide.
    # MIT
    (nsamples, nfeatures) = feature_matrix.shape
    theta = np.zeros(nfeatures)
    theta_sum = np.zeros(nfeatures)
    theta_0 = 0.0
    theta_0_sum = 0.0
    for t in range(T):
        for i in get_order(nsamples):
            theta, theta_0 = perceptron_single_step_update(
                feature_matrix[i], labels[i], theta, theta_0)
            theta_sum += theta
            theta_0_sum += theta_0
    return (theta_sum / (nsamples * T), theta_0_sum / (nsamples * T))
    """
    (nsamples, nfeatures) = feature_matrix.shape
    current_theta = np.zeros(nfeatures)
    current_theta_0 = 0.
    current_theta_acc = current_theta
    current_theta_0_acc = current_theta_0
    for t in range(T):
        for i in prepro.get_order(feature_matrix.shape[0]):
            #print("get_order: \n",i)
            feature_vector = feature_matrix[i]
            label = labels[i]
            current_theta, current_theta_0 = perceptron_single_step_update(feature_vector, label, current_theta, current_theta_0)
            current_theta_acc+=current_theta
            current_theta_0_acc += current_theta_0
    return current_theta_acc/nsamples/T, current_theta_0_acc/nsamples/T

