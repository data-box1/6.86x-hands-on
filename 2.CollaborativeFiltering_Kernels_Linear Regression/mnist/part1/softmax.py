import sys
sys.path.append("..")
import utils
from utils import *
import numpy as np
import matplotlib.pyplot as plt
import scipy.sparse as sparse
#Meas time
import time

#3
def augment_feature_vector(X):
    """
    Adds the x[i][0] = 1 feature for each data point x[i].

    Args:
        X - a NumPy matrix of n data points, each with d - 1 features

    Returns: X_augment, an (n, d) NumPy array with the added feature for each datapoint
    """
    column_of_ones = np.zeros([len(X), 1]) + 1
    return np.hstack((column_of_ones, X))

def compute_probabilities(X, theta, temp_parameter, timeIt=False):
    """
    Computes, for each datapoint X[i], the probability that X[i] is labeled as j
    for j = 0, 1, ..., k-1

    Args:
        X - (n, d) NumPy array (n datapoints each with d features)
        theta - (k, d) NumPy array, where row j represents the parameters of our model for label j
        temp_parameter - the temperature parameter of softmax function (scalar)
    Returns:
        H - (k, n) NumPy array, where each entry H[j][i] is the probability that X[i] is labeled as j
    """
    #YOUR CODE HERE
    #raise NotImplementedError
    t0 = time.time()
    '''
    # 25s
    temp_inv = 1/float(temp_parameter)
    #print ("temp_inv:", temp_inv)
    n = X.shape[0]
    k = theta.shape[0]
    res = []
    for i in range(n):
        arg_exp_vector = (theta @ X[i].T)*temp_inv
        c = np.amax(arg_exp_vector)
        #print("c:", arg_exp_vector)
        #print("max: ", c)
        exp_prod_vector = np.exp(arg_exp_vector-c)
       # print("exp_prod:", exp_prod_vector)
        factor = 1/sum(exp_prod_vector)
        new_column = (factor*exp_prod_vector).reshape(k,1)
        if (i == 0): res = new_column
        else: res = np.append(res, new_column, axis=1)
    #print("final res: ", res)
    '''
    #Optimized solution 0.03s
    n, d = X.shape
    k = theta.shape[0] # # of features
    h = np.dot(theta, X.T) / temp_parameter
    h = h - h.max(axis=0)

    res = np.exp(h)
    res = res / np.sum(res, axis=0)

    if(timeIt): print("compute_probabilities (%f s)"%(time.time() - t0))

    return res

def compute_cost_function(X, Y, theta, lambda_factor, temp_parameter, timeIt=False):
    """
    Computes the total cost over every datapoint.

    Args:
        X - (n, d) NumPy array (n datapoints each with d features)
        Y - (n, ) NumPy array containing the labels (a number from 0-9) for each
            data point
        theta - (k, d) NumPy array, where row j represents the parameters of our
                model for label j
        lambda_factor - the regularization constant (scalar)
        temp_parameter - the temperature parameter of softmax function (scalar)

    Returns
        c - the cost value (scalar)
    """

    #YOUR CODE HERE
    #raise NotImplementedError
    t0 = time.time()
    '''
    H = compute_probabilities(X, theta, temp_parameter)
    k, n = H.shape
    d = theta.shape[1]
    J_1 = 0.
    J_2 = 0.
    
    for i in range(n):
        for j in range(k):
            J_1 += np.log(H[j][i]) if (Y[i]==j) else 0.
    #print("J1: ", (-1.)/(n*1.)*J_1)
    for j in range(k):
        for i in range(d):    
            J_2 += theta[j][i]*theta[j][i]
    #print("J2: ", lambda_factor*.5*J_2)
    cost = (-1.)/(n*1.)*J_1+lambda_factor*.5*J_2
    print("compute_cost_function (%f s)"%(time.time() - t0))
    return cost
    '''
    n, d = X.shape  # datapoints, features

    prob = compute_probabilities(X, theta, temp_parameter)
    loss_term = -np.log(prob[Y, range(n)]).mean()
    reg_term = (1 / 2) * lambda_factor * np.sum(theta ** 2)
    cost = loss_term + reg_term
    if(timeIt): print("compute_cost_function (%f s)"%(time.time() - t0))
    return cost

def run_gradient_descent_iteration(X, Y, theta, alpha, lambda_factor, temp_parameter, timeIt=False):
    """
    Runs one step of batch gradient descent

    Args:
        X - (n, d) NumPy array (n datapoints each with d features)
        Y - (n, ) NumPy array containing the labels (a number from 0-9) for each
            data point
        theta - (k, d) NumPy array, where row j represents the parameters of our
                model for label j
        alpha - the learning rate (scalar)
        lambda_factor - the regularization constant (scalar)
        temp_parameter - the temperature parameter of softmax function (scalar)

    Returns:
        theta - (k, d) NumPy array that is the final value of parameters theta
    """
    #YOUR CODE HERE
    #raise NotImplementedError
    # probability matrix p(y^(i)=j|x^(i);theta)
    H = compute_probabilities(X, theta, temp_parameter) #  (k, n)
    k, n = H.shape
   
    # Spare matrix of [[y(i) == j]]
    M = sparse.coo_matrix(([1]*n, (Y, range(n))), shape=(k,n)).toarray()

    gradJ_theta_m = (-1./(temp_parameter*n*1.))*(M-H)@X + lambda_factor*theta

    #update
    theta -= alpha*gradJ_theta_m
    return theta

def update_y(train_y, test_y):
    """
    Changes the old digit labels for the training and test set for the new (mod 3)
    labels.

    Args:
        train_y - (n, ) NumPy array containing the labels (a number between 0-9)
                 for each datapoint in the training set
        test_y - (n, ) NumPy array containing the labels (a number between 0-9)
                for each datapoint in the test set

    Returns:
        train_y_mod3 - (n, ) NumPy array containing the new labels (a number between 0-2)
                     for each datapoint in the training set
        test_y_mod3 - (n, ) NumPy array containing the new labels (a number between 0-2)
                    for each datapoint in the test set
    """
    #YOUR CODE HERE
    #raise NotImplementedError
    train_y_mod3 = np.mod(train_y, 3)
    #print("train_y: ", train_y)
    #print("train_y_mod3: ", train_y_mod3)

    test_y_mod3 = np.mod(test_y, 3)

    return (train_y_mod3, test_y_mod3)

def compute_test_error_mod3(X, Y_mod3, theta, temp_parameter):
    """
    Returns the error of these new labels when the classifier predicts the digit. (mod 3)

    Args:
        X - (n, d - 1) NumPy array (n datapoints each with d - 1 features)
        Y - (n, ) NumPy array containing the labels (a number from 0-2) for each
            data point
        theta - (k, d) NumPy array, where row j represents the parameters of our
                model for label j
        temp_parameter - the temperature parameter of softmax function (scalar)

    Returns:
        test_error - the error rate of the classifier (scalar)
    """
    #YOUR CODE HERE
    #raise NotImplementedError
    predicted_labels = get_classification(X, theta, temp_parameter)
    predicted_labels_mod3 = np.mod(predicted_labels, 3)
    # equivalent: np.mean(Y_mod3 != predicted_labels_mod3)
    return (1. - np.mean(predicted_labels_mod3 == Y_mod3))

def softmax_regression(X, Y, temp_parameter, alpha, lambda_factor, k, num_iterations, timeIt=False):
    """
    Runs batch gradient descent for a specified number of iterations on a dataset
    with theta initialized to the all-zeros array. Here, theta is a k by d NumPy array
    where row j represents the parameters of our model for label j for
    j = 0, 1, ..., k-1

    Args:
        X - (n, d - 1) NumPy array (n data points, each with d-1 features)
        Y - (n, ) NumPy array containing the labels (a number from 0-9) for each
            data point
        temp_parameter - the temperature parameter of softmax function (scalar)
        alpha - the learning rate (scalar)
        lambda_factor - the regularization constant (scalar)
        k - the number of labels (scalar)
        num_iterations - the number of iterations to run gradient descent (scalar)

    Returns:
        theta - (k, d) NumPy array that is the final value of parameters theta
        cost_function_progression - a Python list containing the cost calculated at each step of gradient descent
    """
    X = augment_feature_vector(X)
    theta = np.zeros([k, X.shape[1]])
    cost_function_progression = []
    t0 = time.time()
    for i in range(num_iterations):
        #t0 = time.time()
        cost_function_progression.append(compute_cost_function(X, Y, theta, lambda_factor, temp_parameter))
        theta = run_gradient_descent_iteration(X, Y, theta, alpha, lambda_factor, temp_parameter)
        #print("softmax_regression iteration %d (%f s)"%(i, time.time() - t0))
    if(timeIt): print("softmax_regression (%f s)"%(time.time() - t0))
    return theta, cost_function_progression

def get_classification(X, theta, temp_parameter):
    """
    Makes predictions by classifying a given dataset

    Args:
        X - (n, d - 1) NumPy array (n data points, each with d - 1 features)
        theta - (k, d) NumPy array where row j represents the parameters of our model for
                label j
        temp_parameter - the temperature parameter of softmax function (scalar)

    Returns:
        Y - (n, ) NumPy array, containing the predicted label (a number between 0-9) for
            each data point
    """
    X = augment_feature_vector(X)
    probabilities = compute_probabilities(X, theta, temp_parameter)
    return np.argmax(probabilities, axis = 0)

def plot_cost_function_over_time(cost_function_history, label = None):
    plt.plot(range(len(cost_function_history)), cost_function_history, label=label)
    plt.ylabel('Cost Function')
    plt.xlabel('Iteration number')
    if(label != None): plt.legend()
    plt.show(block=False)

def compute_test_error(X, Y, theta, temp_parameter):
    error_count = 0.
    assigned_labels = get_classification(X, theta, temp_parameter)
    return 1 - np.mean(assigned_labels == Y)
