import numpy as np
# difference between LinearSVC and SVC(kernel="linear")
#https://stackoverflow.com/questions/45384185/what-is-the-difference-between-linearsvc-and-svckernel-linear
from sklearn.svm import LinearSVC
#from sklearn import svm

### Functions for you to fill in ###
#2
def one_vs_rest_svm(train_x, train_y, test_x):
    """
    Trains a linear SVM for binary classifciation

    Args:
        train_x - (n, d) NumPy array (n datapoints each with d features)
        train_y - (n, ) NumPy array containing the labels (0 or 1) for each training data point
        test_x - (m, d) NumPy array (m datapoints each with d features)
    Returns:
        pred_test_y - (m,) NumPy array containing the labels (0 or 1) for each test data point
    """
    #https://www.datacamp.com/community/tutorials/svm-classification-scikit-learn-python?utm_source=adwords_ppc&utm_medium=cpc&utm_campaignid=1455363063&utm_adgroupid=65083631748&utm_device=c&utm_keyword=&utm_matchtype=&utm_network=g&utm_adpostion=&utm_creative=332602034364&utm_targetid=dsa-429603003980&utm_loc_interest_ms=&utm_loc_physical_ms=1000104&gclid=Cj0KCQjw5-WRBhCKARIsAAId9Fnx3lYdfo7ZZ4UtkOTt_X57DkYp7nfjA-VFQfvJXbRy92UCM76TTXAaAmydEALw_wcB
    #raise NotImplementedError

    #Create a svm Classifier
    #https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html
    # Faster!
    clf = LinearSVC(C= 0.1, random_state=0) # Linear Kernel
    #SVM one vs. rest test_error: 0.007499999999999951

    #clf = svm.SVC(kernel='linear', C= 0.1, random_state=0) # Linear Kernel
    #SVM one vs. rest test_error: 0.007199999999999984
    '''
    C is the penalty parameter, which represents misclassification or error
    term. The misclassification or error term tells the SVM optimization how 
    much error is bearable. This is how you can control the trade-off between
    decision boundary and misclassification term. A smaller value of C creates
    a small-margin hyperplane and a larger value of C creates a larger-margin
    hyperplane.
    '''
    #Train the model using the training sets
    clf.fit(train_x, train_y)
    params = clf.get_params()
    print("Classes (one_vs_rest_svm): ",clf.classes_)
    #Classes (one_vs_rest_svm):  [0 1]

    #Predict the response for test dataset
    y_pred = clf.predict(test_x)
    return y_pred

def multi_class_svm(train_x, train_y, test_x):
    """
    Trains a linear SVM for multiclass classifciation using a one-vs-rest strategy

    Args:
        train_x - (n, d) NumPy array (n datapoints each with d features)
        train_y - (n, ) NumPy array containing the labels (int) for each training data point
        test_x - (m, d) NumPy array (m datapoints each with d features)
    Returns:
        pred_test_y - (m,) NumPy array containing the labels (int) for each test data point
    """
    #raise NotImplementedError
    clf = LinearSVC(C= 0.1, random_state=0) # Linear Kernel
    #Multiclass SVM test_error: 0.08189999999999997
    #clf = svm.SVC(kernel='linear', C= 0.1, random_state=0) # Linear Kernel
    #Multiclass SVM test_error: 0.05259999999999998
    #https://scikit-learn.org/stable/modules/svm.html
    #SVC, NuSVC and LinearSVC are classes capable of performing binary and multi-class classification on a dataset.
    #Train the model using the training sets
    clf.fit(train_x, train_y)
    params = clf.get_params()
    print("Classes (multi_class_svm): ",clf.classes_)
    # Classes:  [0 1 2 3 4 5 6 7 8 9]

    #dec = clf.decision_function(train_x)
    #print("Classes: ",dec.shape) # 4 classes: 4*3/2 = 6
    #Predict the response for test dataset
    y_pred = clf.predict(test_x)
    return y_pred

def compute_test_error_svm(test_y, pred_test_y):
    return 1 - np.mean(pred_test_y == test_y)

